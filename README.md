This repository contains all configuration files etc. for the FL2 game slated to start in early january 2016.

A freeciv-server binary has to be compiled seperately. The vanilla flavour won't do. Instead, do the following:
git clone https://github.com/longturn/freeciv-S2_5.git
git checkout longturn
and in the respective folder, run something like:
./autogen.sh --enable-client=stub --enable-fcmp=no --enable-fcdb=sqlite3 --prefix=SOME_PATH_WHERE_THE_BINARIES_WILL_BE_INSTALLED
and then:
make
