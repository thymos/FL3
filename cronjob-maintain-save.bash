#!/bin/bash

DIR="${BASH_SOURCE%/*}"
if [[ ! -d "$DIR" ]]; then DIR="$PWD";  fi
echo "dir this script is located in: $DIR"

#load configuration
source "$DIR/configuration.sh"
GAMEHOMEDIR="${ROOTPATH}/${GAMENAME}"

#should run every hour to save the current game
bash ${GAMEHOMEDIR}/control.bash maintain
bash ${GAMEHOMEDIR}/control.bash clean
bash ${GAMEHOMEDIR}/control.bash save
