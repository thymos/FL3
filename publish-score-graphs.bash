#!/bin/bash
DIR="${BASH_SOURCE%/*}"
if [[ ! -d "$DIR" ]]; then DIR="$PWD";  fi
echo "dir this script is located in: $DIR"

#load configuration
source "$DIR/configuration.sh"
GAMEHOMEDIR="${ROOTPATH}/${GAMENAME}"

gzip --force --keep "${GAMEHOMEDIR}/freeciv-score.log"
${GAMEHOMEDIR}/freeciv-score-log.py --output="${STATSDIR}" ${GAMEHOMEDIR}/freeciv-score.log.gz
#mv bnp.png  
