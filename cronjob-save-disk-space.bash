#!/bin/bash
DIR="${BASH_SOURCE%/*}"
if [[ ! -d "$DIR" ]]; then DIR="$PWD";  fi
echo "dir this script is located in: $DIR"

#load configuration
source "$DIR/configuration.sh"

SNAPSHOTFOLDER="${ROOTPATH}/${GAMENAME}-stuff/snapshots/"

for FILE in ${SNAPSHOTFOLDER}/*ppm
do
	gzip -f "${FILE}"
done;
